from tkinter import *
from tkinter import messagebox

root = Tk()

# callback
def btn_red_callback():
    #messagebox.showinfo('test', 'red')
    lb_heading.config(foreground='red')

def btn_blue_callback():
    #messagebox.showinfo('test', 'blue')
    lb_heading.config(foreground='blue')

lb_heading = Label(root,
    text='SETEC',
    font=('BatmanForeverAlternate', 30),
    highlightbackground='green',
    highlightthickness=10,
    bd=0,
    padx=30,
    pady=5)
lb_subtitle = Label(root,
    text='Welcome to\nTkinter class',
    font=('Fiolex Girls', 20, 'bold'))

lb_heading.pack(padx=2, pady=2)
lb_subtitle.pack(padx=2, pady=2)

btn_red = Button(root, text='Red',
    command=btn_red_callback)

btn_blue = Button(root, text='Blue',
    command=btn_blue_callback)

btn_red.pack(fill='x', padx=2, pady=2)
btn_blue.pack(fill='x', padx=2, pady=2)

root.configure(background='yellow',
    padx=14, pady=14)
root.geometry('500x500')
root.mainloop()