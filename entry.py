from tkinter import *

root = Tk()

def on_btn_get_clicked():
    print(text_email.get())

text_email = StringVar()

email_entry = Entry(root, textvariable=text_email, show='*')

get_btn = Button(root, text='Get Email', command=on_btn_get_clicked)

email_entry.pack()
get_btn.pack()

# config root form
root.geometry('300x300')

root.mainloop()