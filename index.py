# 1. import package
import tkinter
import tkinter.messagebox

# 2. create root form
root = tkinter.Tk()

def check_sport():
    tkinter.messagebox.showinfo('Result',
        f'Sport = {sport.get()}')

def check_music():
    tkinter.messagebox.showinfo('Result',
        f'Music = {music.get()}')

sport = tkinter.IntVar()
music = tkinter.IntVar()

cb_sport = tkinter.Checkbutton(root,
    text='Sport',
    variable=sport,
    onvalue=1,
    offvalue=0,
    font=('Inter', 25, 'bold'),
    foreground='blue',
    command=check_sport)

cb_music = tkinter.Checkbutton(root,
    text='Music',
    variable=music,
    onvalue=1,
    offvalue=0,
    font=('Inter', 25, 'bold'),
    foreground='red',
    command=check_music)

cb_sport.pack()
cb_music.pack()

cb_sport.select()

# config root form
root.geometry('300x300')

# 3. show root form
root.mainloop()

