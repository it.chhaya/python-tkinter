import tkinter as tk
import tkinter.messagebox as msg

root = tk.Tk()

def btn_gender_callback():

    gender = ''

    if var_gender.get() == 1:
        gender = 'Female'
    else:
        gender = 'Male'
        
    #msg.showinfo('Gender', f'Gender = {gender}')
    lb_var_gender.set(gender)

# declare binding variable
var_gender = tk.IntVar()

lb_var_gender = tk.StringVar()
lb_var_gender.set('Waiting')

rb_female = tk.Radiobutton(root,
    text='Female',
    variable=var_gender,
    value=1,
    font=('Inter', 20))
rb_male = tk.Radiobutton(root,
    text='Male',
    variable=var_gender,
    value=0,
    font=('Inter', 20))

btn_gender = tk.Button(root, text='Get Gender',
    command=btn_gender_callback)

lb_gender = tk.Label(root, textvariable=lb_var_gender,
    font=('Inter', 30))

rb_female.pack()
rb_male.pack()
btn_gender.pack()
lb_gender.pack()

# full screen without bar
# root.attributes('-fullscreen', True)

#getting screen width and height of display
width = root.winfo_screenwidth()
height = root.winfo_screenheight()
root.geometry("%dx%d" % (width, height))

root.title('Radio Button')
root.mainloop()