from tkinter import *

root = Tk()

def on_btn_clicked():
    indexes = list_box.curselection()
    for index in indexes:
        print(list_box.get(index))

list_box = Listbox(root, selectmode=MULTIPLE)
list_box.insert(1, 'Python')
list_box.insert(2, 'Java')
list_box.insert(3, 'Web Design')
list_box.insert(4, 'UI/UX Design')
list_box.insert(5, 'Database')

btn = Button(root, text='Get Data', command=on_btn_clicked)

list_box.pack()
btn.pack()

# config root form
root.geometry('300x300')
root.mainloop()