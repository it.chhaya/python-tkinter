from tkinter import *

root = Tk()
root.geometry("240x100")
root.title('Login')
root.resizable(0, 0)

root.columnconfigure(0, weight=1)
root.columnconfigure(1, weight=3)

username = StringVar()
password = StringVar()

lb_username = Label(root, text='Username')
entry_username = Entry(root, textvariable=username)

lb_password = Label(root, text='Password')
entry_password = Entry(root, textvariable=password)

btn_login = Button(root, text='Log in')

lb_username.grid(column=0, row=0, sticky=W, padx=5, pady=5)
entry_username.grid(column=1, row=0, sticky=E, padx=5, pady=5)
lb_password.grid(column=0, row=1, sticky=W, padx=5, pady=5)
entry_password.grid(column=1, row=1, sticky=E, padx=5, pady=5)
btn_login.grid(column=1, row=3, sticky=E, padx=5, pady=5)

root.mainloop()